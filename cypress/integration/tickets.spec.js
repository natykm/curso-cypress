describe ("Tickets", () => {
	beforeEach (() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input", () => {
		cy.get("#first-name").type("Natalya");
		cy.get("#last-name").type("Kadri");
		cy.get("#email").type("talkingabout@gmail.com");
		cy.get("#requests").type("Vegetarian");
		cy.get("#signature").type("NGK");
	});

	it("select two tickets", () => {
		cy.get("#ticket-quantity").select("2");
	});

	it("select 'vip' ticket type", () => {
		cy.get("#vip").check();
	});

	it("selects 'social media' checkbox", () => {
		cy.get("#social-media").check();
	});

	it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
		cy.get("#friend").check();
		cy.get("#publication").check();
		cy.get("#friend").uncheck();
	});

	it("has 'TICKETBOX' header's heading", () => {
		cy.get("header h1").should("contain", "TICKETBOX")
	});

	it("alerts on invalid email", () => {
		cy.get("#email")
		.as("email")
		.type("talkingabouttesting-gmail.com");

		cy.get("#email.invalid").should("exist");

		cy.get("@email")
		.clear()
		.type("talkingabouttesting@gmail.com");

		cy.get("#email.invalid").should("not.exist");
	});

	/* Teste completo - preencher, confirmar e resetar o formulario*/
	it("fills and reset the form", () => {
		/* variáveis */
		const firstName = "Natalya";
		const lastName = "Kadri";
		const fullName = `${firstName} ${lastName}`;

		/* ações a serem executadas */
		cy.get("#first-name").type("Natalya");
		cy.get("#last-name").type("Kadri");
		cy.get("#email").type("talkingabout@gmail.com");
		cy.get("#ticket-quantity").select("2");
		cy.get("#vip").check();
		cy.get("#friend").check();
		cy.get("#requests").type("Nada a dizer");

		/* verificação (assertions) - verificar se o paragrafo do Termo de Aceite está válido*/
		cy.get(".agreement p").should("contain", `I, ${fullName}, wish to buy 2 VIP tickets`);

		/* ação - clicar no Agrre e preencher o campo signature*/
		cy.get("#agree").click();
		cy.get("#signature").type(fullName);

		/* Verificar se ao preencher todos os campos habilita o botão COnfirm 
		botão com a propriedade type que recebe submit, chamar esse botão de 'submitbutton'
		should preencheu todos os campos obrigatórios e não pode estar desabilitado  */
		cy.get("button[type='submit']")
			.as("submitButton")
			.should("not.be.disabled");

		/* Após verificar que não está desabilitado vamos resetar o formulario clicando no botão Reset*/
		cy.get("button[type='reset']").click();

		/* Após resetar o formulario verificar que o botão Confirm está desabilitado*/
		cy.get("@submitButton").should("be.disabled");
	});

	/* exemplo com comandos customizados - preencher os campos obrigatorios e verifica se o botão Confirm esta ou não habilitado */
	/* variavel com objeto javascrit */
	it("fills mandatory filds using support command", () => {
		const customer = {
			firstName: "João",
			lastName: "Silva",
			email:"joaosilva@example.com",
		};
		/* função customizada que recebe o objeto*/
		cy.fillMandatoryFields(customer);

		/* verificação */
		cy.get("button[type='submit']")
			.as("submitButton")
			.should("not.be.disabled");

		cy.get("#agree").uncheck();

		cy.get("@submitButton").should("be.disabled");
	});

});